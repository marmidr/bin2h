# Introduction

**bin2h** is a small tool for converting binary data into a C-style uchar array for compiling 
with source code.

# Details 

A small (and common) tool for converting binary data to a C-style uchar array 
for compiling straight into C/C++ code.

Originally written because I couldn't find what I was looking for with Google. 
It is very similar to the one found on 
http://www.programmersheaven.com/download/16517/download.aspx,
I knocked up a fresh version from scratch since the linked is a 16bit binary 
(won't run on a 64bit system) and the code is a little out of date now.

Published in the hope that it will save someone an hour or two.

Compiling binary data into an executable can be useful for a whole variety of reasons, 
such as a need for quick access after process start-up (e.g. splash screens) or guaranteed 
availability (e.g. optical media or network/cloud based software). 

**bin2h** is intended to be a reasonable approximation of a small, sharp tool 
to convert any binary file into a format suitable for embedding in the executable binary.


# Usage 

`bin2h in-file out-file optional-args`

## Possible arguments:

 * out-file is optional, if not specified output is directed to cout
 * -id=_name_ if specified, the identifier of the C array is _name_. The identifier is _data_ if this argument is not specified.
 * -ns=_name_ if specified, the C array is placed inside a C++ namespace called _name_. If not specified, the C array is placed in the global namespace.

